window.onload = function () {

    const items = document.querySelectorAll('.object'); /*crée un tableau */
    const nbSlide = items.length; /* 5 */
    const suivant = document.querySelector('.right');
    const precedent = document.querySelector('.left');

    let count = 0; 

    function slideSuivant(){
        items[count].classList.remove('active');
        items[count].classList.remove('a1');
        items[count+1].classList.remove('a2');
        items[count+2].classList.remove('a3');

        if (count < nbSlide - 1){
            count++;
            console.log(count);
        } else {
            count = 0 
            console.log(count);
        }
        items[count].classList.add('a1');
        items[count].classList.add('active');
        items[count+1].classList.add('a2');
        items[count+1].classList.add('active');
        items[count+2].classList.add('a3');
        items[count+2].classList.add('active');
    }

    function slidePrecedent(){
        items[count+2].classList.remove('active');
        items[count+2].classList.remove('a3');
        items[count+1].classList.remove('a2');
        items[count].classList.remove('a1');

        if (count > 0){
            count--;
            console.log(count);
        } else {
            count = nbSlide -1;
            console.log(count);
        }
        items[count].classList.add('active');
        items[count].classList.add('a1');
        items[count+1].classList.add('active');
        items[count+1].classList.add('a2');
        items[count+2].classList.add('active');
        items[count+2].classList.add('a3');
    }

    suivant.addEventListener("click", slideSuivant);
    precedent.addEventListener("click", slidePrecedent);

    suivant.addEventListener('click', labelSuivant);
    precedent.addEventListener('click', labelPrecedent);


    const labels = document.querySelectorAll('.label');
    const nbLabels = labels.length;

    let countLabel = 1;

    function labelSuivant (){
        labels[countLabel].classList.remove('labelactive');

        if (countLabel < nbLabels - 1){
            countLabel ++ ;
            console.log('countLabel : ' + countLabel);
        } else {
            countLabel = 0;
        }
        labels[countLabel].classList.add("labelactive");
    }

    function labelPrecedent(){
        labels[countLabel].classList.remove("labelactive");

        if (countLabel > 0){
            countLabel --;
            console.log('countLabel : ' + countLabel);
        } else {
            countLabel = nbLabels - 1;
        }

        labels[countLabel].classList.add('labelactive');
    }
}